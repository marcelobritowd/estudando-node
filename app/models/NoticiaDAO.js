class NoticiaDAO {
    constructor(connection) {
        this.connection = connection;
    }

    getNoticias(callback) {
        this.connection.query(`select * from noticias`, callback);
    };

    getNoticia (id, callback) {
        this.connection.query(`select * from noticias where id_noticia = ${id}`, callback);
    };

    addNoticia (noticia, callback) {
        connection.query('insert into noticias set ? ' , noticia, callback);
    };
}



module.exports = () => {
    return NoticiaDAO;
}