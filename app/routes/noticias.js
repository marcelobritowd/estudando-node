module.exports = application => {

    application.get('/noticias', (req, res) => {
        let connection = application.config.dbConnection();
        let noticiaModel = new application.app.models.NoticiaDAO(connection);

        noticiaModel.getNoticias((error, result) => {
            res.render('noticias/noticias', {noticias: result});
        })
    });
}