module.exports = application => {

    application.get('/noticias/:id', (req, res) => {
        let connection = application.config.dbConnection();   
        let noticiaModel = new application.app.models.NoticiaDAO(connection);

        noticiaModel.getNoticia(req.params.id, (error, result) => {
            res.render('noticias/noticia', {noticia: result});
        })

    })
}