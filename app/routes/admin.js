module.exports = application => {
    application.get('/formulario_inclusao_noticia', (req, res) => {
        res.render('admin/form_add_noticia');
    })

    application.post('/noticias/salvar', (req, res) => {
        let connection = application.config.dbConnection();
        let noticiaModel = new application.app.models.NoticiaDAO(connection);
        const noticia = req.body;

        noticiaModel.addNoticia(noticia, (error, result) => {
            res.redirect('/noticias');
        })
    });
}